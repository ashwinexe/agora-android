package org.aossie.agoraandroid.data.db.model

data class Winner(
  val candidate: Candidate? = null,
  val score: Score? = null
)
