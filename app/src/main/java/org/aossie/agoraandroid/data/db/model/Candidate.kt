package org.aossie.agoraandroid.data.db.model

data class Candidate(
  val name: String? = null,
  val id: String? = null,
  val party: String? = null
)
